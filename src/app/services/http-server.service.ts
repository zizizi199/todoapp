import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HttpServerService {
    public REST_API_SERVER = 'http://192.168.1.16:5000';
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization:'Bearer '+ localStorage.getItem('token') ||''
    }),
  };

  constructor() { }
}
