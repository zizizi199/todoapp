import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpServerService } from './http-server.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private router: Router,
    private HttpServerService:HttpServerService,
    private httpClient: HttpClient
  ) {}
  private arr: any = localStorage.getItem('userArray')
  private userArray =
    JSON.parse(this.arr) ||
    [{
      userName: 'admin',
      password: '123'
    }]

  authUser(user: any) {
    const url = `${this.HttpServerService.REST_API_SERVER}/auth/login`;
    let input ={
      username: user?.userName,
      password: user?.password
    }
    
   this.httpClient
   .post<any>(url,input ,this.HttpServerService.httpOptions)
   .subscribe(data => {
    localStorage.setItem('token',data?.access_token)
				alert('Đăng nhập thành công')
        this.router.navigate(['home'])
			});
  }

  registerUser(user: any) {
    const url = `${this.HttpServerService.REST_API_SERVER}/auth/regiser`;
    let input ={
      username: user?.userName,
      password: user?.password
    }
    
   this.httpClient
   .post<any>(url,input ,this.HttpServerService.httpOptions)
   .subscribe(data => {
				alert('Đăng kí thành công')
        this.router.navigate(['login'])
			});
  }
  //   LogoutUser(user: any) {
  //   const url = `${this.HttpServerService.REST_API_SERVER}/auth/regiser`;
  //   let input ={
  //     username: user?.userName,
  //     password: user?.password
  //   }
    
  //  this.httpClient
  //  .post<any>(url,input ,this.HttpServerService.httpOptions)
  //  .subscribe(data => {
	// 			alert('Đăng kí thành công')
  //       this.router.navigate(['login'])
	// 		});
  // }
}
