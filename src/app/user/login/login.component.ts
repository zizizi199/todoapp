import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {  Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router ,
    ) { }

  ngOnInit(): void {
  }
  onLogin(loginForm: NgForm) {
    console.log(loginForm.value);
    this.authService.authUser(loginForm.value)
    // if (user) {
    //   alert('Đăng nhập thành công');
    //   this.router.navigate(['home'])
    //   localStorage.setItem('userName',loginForm.value?.userName )
    // } else {
    //   alert('Đăng nhập không thành công');
      
    // }
  }

}
