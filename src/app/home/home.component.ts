import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpServerService } from '../services/http-server.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(
    private router: Router,
    private HttpServerService: HttpServerService,
    private httpClient: HttpClient,
  ) { }

  public list: any = [];
  itemList: any = '';
  ngOnInit(): void {
    this.getAllItem()
  }

  public getAllItem() {
    const url = `${this.HttpServerService.REST_API_SERVER}/api/note`;
    this.httpClient
      .get<any>(url, this.HttpServerService.httpOptions)
      .subscribe(data => {
        this.list = data?.data
      });
  }

  public reset() {
    this.itemList = ''
  }

  public addItem(value: any) {
    let input = {
      note: value
    }
    const url = `${this.HttpServerService.REST_API_SERVER}/api/note`;
    this.httpClient
      .post<any>(url, input, this.HttpServerService.httpOptions)
      .subscribe(
        data => {
          {
            this.getAllItem()
          }
        },
        error => console.log('oops', error)
      );
    this.reset()
  }

  public removeItem(value: any) {
     const url = `${this.HttpServerService.REST_API_SERVER}/api/note/${value}`;
    this.httpClient
      .delete<any>(url, this.HttpServerService.httpOptions)
      .subscribe(
        data => {
          {
            this.getAllItem()
          }
        },
        error => console.log('oops', error)
      );
    this.reset()
  }

  public doneItem(value: any) {
    this.list[value].done = true
  }

  onLogout() {
    this.router.navigate([''])
    localStorage.removeItem('userName')
  }


}
